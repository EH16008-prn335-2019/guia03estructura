#include <stdio.h>
#include <stdlib.h>

int main() {
    int tamanio;
    int i;
    int *puntero;
    printf("Ingrese el tamaño del vector: ");
    scanf("%d",&tamanio);
    int v[tamanio];
    printf("\nImgrese los valores del vector:\n");
    for (i = 0; i < tamanio; i++) {
        printf("posicion %d: ",i+1);
        scanf("%d",&v[i]);
    }
    puntero=&v;

    printf("\nVector de la manera ingresada:\n[");
    for (i = 0; i < tamanio; i++) {
        printf(" %d ",*puntero);
        puntero++;
    }
    printf("]");
    
    printf("\nVector en sentido inverso:\n[");
    for (i = 0; i < tamanio; i++) {
        puntero--;
        printf(" %d ",*puntero);
        
    }
    printf("]");
    return 0;
}
